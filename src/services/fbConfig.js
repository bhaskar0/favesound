import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
  apiKey: 'AIzaSyAaAfkCxscsb8YoWFvObNDzyc28jbyR004',
  authDomain: 'favesound-55989.firebaseapp.com',
  databaseURL: 'https://favesound-55989.firebaseio.com',
  projectId: 'favesound-55989',
  storageBucket: 'favesound-55989.appspot.com',
  messagingSenderId: '348202790289',
  appId: '1:348202790289:web:23edafb342adea9ad1fe30',
  measurementId: 'G-Z9MWHN2N45',
};

firebase.initializeApp(config);
firebase.firestore().settings({ timeStampsInSnapshots: true });
export default firebase;
