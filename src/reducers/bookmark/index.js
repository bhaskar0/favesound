import * as actionTypes from '../../constants/actionTypes';

const initialState = {
  bookmarkedSongIds: [],
};

export default function addBookMarkReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.BOOKMARK: {
      return {
        ...state,
        bookmarkedSongIds: [...action.payload.id],
      };
    }
    case actionTypes.GET_BOOKMARKED_SONGS: {
      return {
        ...state,
        bookmarkedSongIds: [...action.payload.id],
      };
    }
    case actionTypes.REMOVE_BOOKMARK_SONG: {
      return {
        ...state,
        bookmarkedSongIds: [...action.payload.id],
      };
    }
  }
  return state;
}
