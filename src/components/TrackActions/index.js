import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import * as actions from '../../actions/index';
import ButtonGhost from '../../components/ButtonGhost';
import { withFirebase } from 'react-redux-firebase';

function TrackActions({
  onOpenComments,
  onAddTrackToPlaylist,
  onLogin,
  userId,
  onAddBookmark,
  firebase,
  activity,
  bookmarkedIds,
  removeBookmarkedSongs,
}) {
  const [bookmark, setBookmark] = useState(false);
  const isSmall = true;

  useEffect(() => {
    if (bookmarkedIds.includes(activity.id)) {
      setBookmark(true);
    }
  }, [bookmarkedIds]);

  function onhandleBookmark() {
    if (userId && userId.id) {
      onAddBookmark(activity.id, userId.id, firebase);
      setBookmark(true);
    } else {
      onLogin();
    }
  }
  function onhandleBookmarkremove() {
    if (userId && userId.id) {
      removeBookmarkedSongs(activity.id, userId.id, firebase);
      setBookmark(false);
    } else {
      onLogin();
    }
  }
  return (
    <div className="track-actions-list">
      <div className="track-actions-list-item">
        {bookmark ? (
          <ButtonGhost isSmall={isSmall} onClick={onhandleBookmarkremove}>
            <i className="fa fa-bookmark" /> Bookmarked
          </ButtonGhost>
        ) : (
          <ButtonGhost isSmall={isSmall} onClick={onhandleBookmark}>
            <i className="fa fa-bookmark" /> Bookmark
          </ButtonGhost>
        )}
      </div>
      <div className="track-actions-list-item">
        <ButtonGhost isSmall={isSmall} onClick={onAddTrackToPlaylist}>
          <i className="fa fa-th-list" /> Add to Playlist
        </ButtonGhost>
      </div>
      <div className="track-actions-list-item">
        <ButtonGhost isSmall={isSmall} onClick={onOpenComments}>
          <i className="fa fa-comment" /> Comment
        </ButtonGhost>
      </div>
    </div>
  );
}

function mapStateToProps(state, props) {
  return {
    activity: props.activity,
    userId: state.session.user,
    songs: state.entities.tracks,
    bookmarkedIds: state.bookMark.bookmarkedSongIds,
  };
}

function mapDispatchToProps(dispatch, props) {
  const { activity } = props;

  return {
    onOpenComments: () =>
      bindActionCreators(actions.openComments, dispatch)(activity.id),
    onAddTrackToPlaylist: () =>
      bindActionCreators(actions.addTrackToPlaylist, dispatch)(activity),
    onAddBookmark: (id, userId, firebase) =>
      bindActionCreators(actions.addBookMark, dispatch)(id, userId, firebase),
    onLogin: bindActionCreators(actions.login, dispatch),
    removeBookmarkedSongs: (trackId, userId, firebase) =>
      bindActionCreators(actions.removeBookmarkedSongs, dispatch)(
        trackId,
        userId,
        firebase,
      ),
  };
}

TrackActions.propTypes = {
  onOpenComments: PropTypes.func,
  onAddTrackToPlaylist: PropTypes.func,
};

export default compose(
  withFirebase,
  connect(mapStateToProps, mapDispatchToProps),
)(TrackActions);
