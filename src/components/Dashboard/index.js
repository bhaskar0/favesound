import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as requestTypes from '../../constants/requestTypes';
import { TrackStreamContainer } from '../Track';
import { compose, bindActionCreators } from 'redux';
import { withFirebase } from 'react-redux-firebase';
import * as actions from '../../actions/index';

class Dashboard extends React.Component {
  componentDidUpdate(prevProps) {
    if (
      this.props.user !== prevProps.user ||
      this.props.bookmarkedSongIds !== prevProps.bookmarkedSongIds
    ) {
      this.props.getBookmarkedSongs(this.props.user.id, this.props.firebase);
    }
  }

  render() {
    const { isAuthInProgress, isAuthed, songs, bookmarkedSongIds } = this.props;

    const bookmarkedSongs = Object.values(songs).filter((song) =>
      bookmarkedSongIds.includes(song.id),
    );

    if (isAuthInProgress) {
      return null;
    }

    if (!isAuthed) {
      return <Redirect to="/" />;
    }

    return (
      <div>
        <div className="dashboard">
          <div className="dashboard-main">
            {bookmarkedSongs.map((song, index) => (
              <TrackStreamContainer activity={song} key={index} />
            ))}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  isAuthed: Boolean(state.session.session),
  isAuthInProgress: state.request[requestTypes.AUTH],
  bookmarkedSongIds: state.bookMark.bookmarkedSongIds,
  songs: state.entities.tracks,
  user: state.session.user,
});

function mapDispatchToProps(dispatch) {
  return {
    getBookmarkedSongs: (userid, firebase) =>
      bindActionCreators(actions.getBookmarkedSongs, dispatch)(
        userid,
        firebase,
      ),
  };
}

export default compose(
  withFirebase,
  connect(mapStateToProps, mapDispatchToProps),
)(Dashboard);
