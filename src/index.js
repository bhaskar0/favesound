/* eslint-disable */
import SC from 'soundcloud';
import { AppContainer } from 'react-hot-loader';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
/* eslint-enable */

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import configureStore from './stores/configureStore';
import App from './components/App';
import { ReactReduxFirebaseProvider } from 'react-redux-firebase';
import { createFirestoreInstance } from 'redux-firestore';

require('../styles/index.scss');

const firebaseConfig = {
  apiKey: 'AIzaSyAaAfkCxscsb8YoWFvObNDzyc28jbyR004',
  authDomain: 'favesound-55989.firebaseapp.com',
  databaseURL: 'https://favesound-55989.firebaseio.com',
  projectId: 'favesound-55989',
  storageBucket: 'favesound-55989.appspot.com',
  messagingSenderId: '348202790289',
  appId: '1:348202790289:web:23edafb342adea9ad1fe30',
  measurementId: 'G-Z9MWHN2N45',
};
const rrfConfig = {
  userProfile: 'users',
  useFirestoreForProfile: true,
};
firebase.initializeApp(firebaseConfig);
firebase.firestore();
const store = configureStore();

const rrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch,
  createFirestoreInstance,
};

// function render(Component) {
ReactDOM.render(
  <AppContainer>
    <Provider store={store}>
      <ReactReduxFirebaseProvider {...rrfProps}>
        <App />
      </ReactReduxFirebaseProvider>
    </Provider>
  </AppContainer>,
  document.getElementById('app'),
);
// }

// render(App);

// if (module.hot) {
//   module.hot.accept('./components/App', () => {
//     // eslint-disable-next-line
//     const NextApp = require('./components/App').default;
//     render(NextApp);
//   });
// }
