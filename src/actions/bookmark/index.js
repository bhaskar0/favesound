import * as actionTypes from '../../constants/actionTypes';

export const addBookMark = (trackId, userId, firebase) => (dispatch) => {
  const doc = firebase
    .firestore()
    .collection('projects')
    .doc(userId.toString());

  doc.get().then((docSnapshot) => {
    if (docSnapshot.exists) {
      doc
        .update({ id: firebase.firestore.FieldValue.arrayUnion(trackId) })
        .then(() =>
          doc.get().then((data) =>
            dispatch({
              type: actionTypes.BOOKMARK,
              payload: data.data(),
            }),
          ),
        );
    } else {
      doc
        .set({ id: [trackId] })
        .then(() =>
          dispatch({ type: actionTypes.BOOKMARK, payload: { trackId } }),
        );
    }
  });
};

export const getBookmarkedSongs = (userId, firebase) => (dispatch) => {
  const doc = firebase
    .firestore()
    .collection('projects')
    .doc(userId.toString());

  doc.get().then((docSnapshot) => {
    if (docSnapshot.exists) {
      dispatch({
        type: actionTypes.GET_BOOKMARKED_SONGS,
        payload: docSnapshot.data(),
      });
    }
  });
};

export const removeBookmarkedSongs = (trackId, userId, firebase) => (
  dispatch,
) => {
  const doc = firebase
    .firestore()
    .collection('projects')
    .doc(userId.toString());

  doc
    .update({ id: firebase.firestore.FieldValue.arrayRemove(trackId) })
    .then(() =>
      doc.get().then((data) =>
        dispatch({
          type: actionTypes.REMOVE_BOOKMARK_SONG,
          payload: data.data(),
        }),
      ),
    );
};
